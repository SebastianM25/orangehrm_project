@SmokeFeature
Feature: feature to test login functionality
@smoketest
  Scenario: Check login is successful with valid credentials
    Given I am on the login page
    When I introduce username and password
    And I click on the login button
    Then I navigated to the home page

