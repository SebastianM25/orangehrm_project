package StepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps {

    @Given("I am on the login page")
    public void i_am_on_the_login_page() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("1");
    }

    @When("I introduce username and password")
    public void i_introduce_username_and_password() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("2");
    }

    @And("I click on the login button")
    public void i_click_on_the_login_button() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("3");
    }

    @Then("I navigated to the home page")
    public void i_navigated_to_the_home_page() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("4");
    }

}
